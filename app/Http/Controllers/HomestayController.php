<?php

namespace App\Http\Controllers;
use App\homestay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class HomestayController extends Controller
{
    public function home(){
      $show = DB::select('select * from homestays');
      return view('/Homestay/homestay',compact('show'));
    }
    public function GoToForm(){
      return view('/Homestay/AddHomestay');
    }
    public function tambahHomestay(Request $req){
      $homestay = new homestay();

      $homestay->nama_homestay=$req->nama_homestay;
      $homestay->alamat=$req->alamat;
      $homestay->harga = $req->harga;
      $homestay->jumlah_kamar = $req->jumlah_kamar;
      $homestay->area_terdekat=$req->area_terdekat;

      $file = $req->file('gambar');
      $filename = $file->getClientOriginalName();
      $destination = $req->file('gambar')->store('');
      $req->file('gambar')->move('images',$destination);
      $homestay->gambar = $destination;
      $homestay->save();

      return redirect('/Homestay');
    }
    public function edit($id){
      $edit = DB::table('homestays')->where('id',$id)->get();
      return view('Homestay.EditHomestay',compact('edit'));
    }
}
