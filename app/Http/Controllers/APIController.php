<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\homestay;
use App\pemensanan;
use Illuminate\Support\Facades\DB;

class APIController extends Controller
{
    public function home(){
        $show = DB::select('select * from homestays');
        return $show;
      }
      public function detailVIew($id){
          $show = homestay::find($id);
            return $show;
      }
      public function tambahPesanan(Request $request){
          $pesan = new pemensanan();

          $pesan->nama_pemesan = $request->nama_pemesan;
          $pesan->tanggal_checkin = $request->tanggal_checkout;
          $pesan->tanggal_checkout = $request->tanggal_checkout;
          $pesan->nama_homestay = $request->nama_homestay;
          $pesan->id_customer = $request->id_customer;

          return $pesan;
      }
}
