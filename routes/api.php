<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



//routing untuk homestay
Route::get('/Homestay','APIController@home');
Route::get('/detailView/{id}','APIController@detailView');
Route::post('/tambahPesanan','APIController@tambahPesanan');
