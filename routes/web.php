<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/b', "Test@test1");

Auth::routes();

//Routing untuk autentikasi
Route::get('/home', 'HomeController@index')->name('home');

//Routing untuk admin
Route::get('/RegAdmin','HomeController@RegAdmin');

//Routing untuk pemesanan
Route::get('/dataPemesan','HomeController@ShowData');

//Routing untuk wisata
Route::get('/AddWisata','wisataController@AddWisata');

//routing untuk homestay
Route::get('/Homestay','HomestayController@home');
Route::get('/AddHomestay','HomestayController@GoToForm');
Route::post('/tambahHomestay','HomestayController@tambahHomestay');
Route::get('/edit/{id}','HomestayController@edit');
