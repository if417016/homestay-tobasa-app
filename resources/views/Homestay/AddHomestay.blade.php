@extends('layouts.app')

@section('content')
<style media="screen">
#navside{
  background-color:  #32EF7E;
}
input[type=submit]:hover, select{
  background-color: #0000FF;
}
</style>


            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
              </li>
              <li class="breadcrumb-item active">Overview</li>
            </ol>
            <div class="card-body">
                <form method="POST" action="/tambahHomestay" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <label for="nama_homestay" class="col-md-4 col-form-label text-md-right">{{ __('Name Homestay') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="nama_homestay" value="{{ old('name') }}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat Homestay') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="alamat" value="{{ old('email') }}">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="alamat" class="col-md-4 col-form-label text-md-right">Jumlah Kamar</label>

                        <div class="col-md-6">
                            <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="jumlah_kamar" value="{{ old('email') }}">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="alamat" class="col-md-4 col-form-label text-md-right">Harga</label>

                        <div class="col-md-6">
                            <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="harga" value="{{ old('email') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat" class="col-md-4 col-form-label text-md-right">Harga</label>

                        <div class="col-md-6">
                            <input id="email" type="file" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="gambar" value="{{ old('email') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                      <label for="area_terdekat">Area Terdekat</label>
                      <textarea name="area_terdekat" class="form-control" rows="7" placeholder="Masukkan Daerah Wisata Terdekat, Tempat Belanja Terdekat, Reataurant Terdekat"></textarea>
                    </div>
                    <input style="margin-left : 1200px; background-color : #00FF00; height : 50px; border-radius : 7px;
                    border-top-right-radius: 7px;
                    border-bottom-right-radius: 7px;
                    border-bottom-left-radius: 7px;
                    border-top-left-radius: 7px;" type="submit" name="" value="Tambah Homestay">
                </form>
            </div>

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="login.html">Logout</a>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript-->

</div>

@endsection
