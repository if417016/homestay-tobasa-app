<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemensanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemensanans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_pemesan');
            $table->string('tanggal_checkin');
            $table->string('tanggal_checkout');
            $table->string('jumlah_orang');
            $table->string('nama_homestay');
            $table->string('id_customer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemensanans');
    }
}
